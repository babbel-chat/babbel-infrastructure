# infrastructure


## Install microk8s
For further information please consult https://microk8s.io/docs.
As operating system for microk8s we used Ubuntu 20.04.3 LTS.

Install microk8s with Kubernetes version 1.21

``` shell
sudo snap install microk8s --classic --channel=1.21
```

Set repuired permissions.

``` shell
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
```

Set linux alias for better handling.

``` shell
echo "alias kubectl='microk8s kubectl'" > ~/.bash_aliases
```

Reboot system.

``` shell
sudo reboot
```

## Enable microk8s addon
For further information please consult https://microk8s.io/docs/addons.
In our use case we enable the following microk8s addons.

| Addon   | Description                                                                   |
|---------|-------------------------------------------------------------------------------|
| DNS     | Deploys CoreDNS to supply address resolution services to Kubernetes           |
| Ingress | Deploys an NGINX Ingress Control for external communication                   |
| Storage | Create a default storage class which allocates storage from a host directory. |

``` shell
microk8s enable dns ingress storage
```

## Create namespace

``` shell
kubectl create namespace babbel
```

Change your namespace from using `default` to `babbel`.
For all resources for which no explicit namespace was set, are now created in namespace `babbel` 


``` shell
kubectl config set-context --current --namespace=babbel
```


## Create Persistent Volume Claim

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: babbel-mongo-pvc
  labels:
    app: babbel
spec:
  accessModes:
  - ReadWriteOnce # RW from a node
  storageClassName: microk8s-hostpath  # provided by microk8s storage addon
  resources:
    requests:
      storage: 10Gi # requested storage size
EOF
```

## Create secrets for babbel-mongo deployment
``` shell
  kubectl create secret generic babbel-mongo-secret \
  --from-literal='mongo-root-username=xxxxxxxx' \
  --from-literal='mongo-root-password=xxxxxxxx'
```

## Create  babbel-mongo deployment and service

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: babbel-mongo
  labels:
    app: babbel
spec:
  replicas: 1
  selector:
    matchLabels:
      pod-label: babbel-mongo-pod
  template:
    metadata:
      labels:
        pod-label: babbel-mongo-pod
    spec:
      containers:
      - name: babbel-mongo
        image: mongo:4.2.15-bionic
        ports:
        - containerPort: 27017
        env:
        - name: MONGO_INITDB_ROOT_USERNAME
          valueFrom:
            secretKeyRef:
              name: babbel-mongo-secret
              key: mongo-root-username
        - name: MONGO_INITDB_ROOT_PASSWORD
          valueFrom: 
            secretKeyRef:
              name: babbel-mongo-secret
              key: mongo-root-password
        volumeMounts:
          - mountPath: "/data/db"
            name: babbel-mongo-storage
            subPath: babbel-mongo-data
      volumes:
        - name: babbel-mongo-storage
          persistentVolumeClaim:
            claimName: babbel-mongo-pvc
---
apiVersion: v1
kind: Service
metadata:
  name: babbel-mongo
spec:
  selector:
    pod-label: babbel-mongo-pod
  ports:
    - protocol: TCP
      port: 27017
EOF
```


## Create babbel-backend deployment and service

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: babbel-backend
  labels:
    app: babbel
spec:
  replicas: 1
  selector:
    matchLabels:
      app: babbel-backend-pod
  template:
    metadata:
      labels:
        app: babbel-backend-pod
    spec:
      containers:
      - name: babbel-backend
        image: diamjk/babbel-backend:1.0.1
        ports:
        - containerPort: 8080
---
apiVersion: v1
kind: Service
metadata:
  name: babbel-backend
  labels:
    app: babbel
spec:
  selector:
    app: babbel-backend-pod
  ports:
    - protocol: TCP
      port: 80
      targetPort: 8080
EOF
```


##  Create babbel-frontend deplyoment and service

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: babbel-frontend
  labels:
    app: babbel
spec:
  replicas: 1
  selector:
    matchLabels:
      pod-label: babbel-frontend-pod
  template:
    metadata:
      labels:
        pod-label: babbel-frontend-pod
    spec:
      containers:
      - name: babbel-frontend
        image: nlangh/babbel-frontend:latest
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: babbel-frontend
  labels:
    app: babbel
spec:
  selector:
    pod-label: babbel-frontend-pod
  ports:
  - protocol: TCP
    port: 80
EOF
```



## Create babbel-mongo-express and service

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: apps/v1
kind: Deployment
metadata:
  name: babbel-mongo-express
  labels:
    app: babbel
spec:
  replicas: 1
  selector:
    matchLabels:
      pod-label: babbel-mongo-express-pod
  template:
    metadata:
      labels:
        pod-label: babbel-mongo-express-pod
    spec:
      containers:
      - name: babbel-mongo-express
        image: mongo-express
        ports:
        - containerPort: 8081
        env:
        - name: ME_CONFIG_MONGODB_ADMINUSERNAME
          valueFrom:
            secretKeyRef:
              name: babbel-mongo-secret
              key: mongo-root-username
        - name: ME_CONFIG_MONGODB_ADMINPASSWORD
          valueFrom:
            secretKeyRef:
              name: babbel-mongo-secret
              key: mongo-root-password
        - name: ME_CONFIG_MONGODB_SERVER
          value: babbel-mongo
---
apiVersion: v1
kind: Service
metadata:
  name: babbel-mongo-express
  labels:
    app: babbel
spec:
  selector:
    pod-label: babbel-mongo-express-pod
  ports:
    - protocol: TCP
      port: 8081
EOF
```


## Install cert-manager
cert-manager adds certificates and certificate issuers as resource types in Kubernetes clusters, and simplifies the process of obtaining, renewing and using those certificates.


``` shell
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.5.3/cert-manager.yaml
```


## Create Let´s encrypt issuer

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: cert-manager.io/v1
kind: Issuer
metadata:
  name: letsencrypt-prod
spec:
  acme:
    # The ACME production server
    server: https://acme-v02.api.letsencrypt.org/directory
    # Email address used for ACME registration
    email: alexander.walter@sva.de
    # Name of a secret used to store the ACME account private key
    privateKeySecretRef:
      name: letsencrypt-prod
    # Enable the HTTP-01 challenge provider
    solvers:
    - http01:
        ingress:
          class:  public
EOF
```
## Restrict chat page with Basic Authentication

``` shell
sudo apt install -y apache2-utils
```

create a base64 encoded password for user sva
``` shell
htpasswd -c auth sva
``` 

Create a secret is kubernetes from your auth file.

``` shell
kubectl create secret generic basic-auth --from-file=auth
```

## Create Ingress route

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: babbel-ingress-tls
  annotations:
    kubernetes.io/ingress.class: "public"    
    cert-manager.io/issuer: "letsencrypt-prod"
    # type of authentication
    nginx.ingress.kubernetes.io/auth-type: basic
    # name of the secret that contains the user/password definitions
    nginx.ingress.kubernetes.io/auth-secret: basic-auth
    # message to display with an appropriate context why the authentication is required
    nginx.ingress.kubernetes.io/auth-realm: 'Authentication Required for babbel-frontend'
    ## config for websockets
    nginx.ingress.kubernetes.io/proxy-read-timeout: "1800"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "1800"
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/secure-backends: "true"
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
    nginx.ingress.kubernetes.io/websocket-services: core-service
    nginx.org/websocket-services: core-service
spec:
  tls:
  - hosts:
    - chat.kinkara.de
    - mongo-express.kinkara.de
    secretName: babbel-secret-tls
  rules:
  - host: chat.kinkara.de
    http:
      paths:
      - pathType: Prefix
        path: /
        backend:
          service:
            name: babbel-frontend
            port:
              number: 80
  - host: mongo-express.kinkara.de
    http:
      paths:
      - pathType: Prefix
        path: /
        backend:
          service:
            name: babbel-mongo
            port:
              number: 27017
EOF
```




## Create Ingress route no TLS

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: babbel-ingress-tls
  annotations:
    kubernetes.io/ingress.class: "public"  
    ## config for websockets
    nginx.ingress.kubernetes.io/proxy-read-timeout: "1800"
    nginx.ingress.kubernetes.io/proxy-send-timeout: "1800"
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/websocket-services: core-service
    nginx.org/websocket-services: core-service  
spec:
  rules:
  - host: chat.kinkara.de
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: babbel-frontend
            port:
              number: 80
  - host: mongo-express.kinkara.de
    http:
      paths:
      - pathType: Prefix
        path: /
        backend:
          service:
            name: babbel-mongo-express
            port:
              number: 8081
EOF
```


## Nodeport for debug

``` yaml
cat <<EOF | kubectl apply -f -
apiVersion: v1
kind: Service
metadata:
  name: babbel-backend-nodeport
spec:
  type: NodePort
  selector:
    pod-label: babbel-backend-pod
  ports:
    - port: 8080
      targetPort: 8080
      nodePort: 30007
EOF
```
